<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::resource('home', 'HomeController');
Route::resource('client', 'ClientController');
Route::resource('consultant', 'ConsultantController');
Route::resource('login', 'LoginController');
Route::resource('inner', 'InnerController');
Route::resource('vendors', 'VendorController');

Route::get('consultant/consultants_by_categorories_id', 'ConsultantController@consultantsByCategororiesId');