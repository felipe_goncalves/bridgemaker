<!DOCTYPE html>
<html lang="pt">
    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <title>Bridge Maker</title>
        <meta content="" name="descriptison">
        <meta content="" name="keywords">
        <!-- Favicons -->
        <link href="./img/favicon-96x96.png" rel="icon">
        <link href="./img/apple-touch-icon.png" rel="apple-touch-icon">
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
        <!-- Vendor CSS Files -->
        <link href="./vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="./vendor/icofont/icofont.min.css" rel="stylesheet">
        <link href="./vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
        <link href="./vendor/venobox/venobox.css" rel="stylesheet">
        <link href="./vendor/animate.css/animate.min.css" rel="stylesheet">
        <link href="./vendor/remixicon/remixicon.css" rel="stylesheet">
        <link href="./vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="./vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
        <!-- Template Main CSS File -->
        <link href="./css/style.css" rel="stylesheet">
    </head>
    <body>
        <header id="header" class="fixed-top">
            <div class="container d-flex align-items-center">
            <a href="#hero" class="logo mr-auto"><img src="./img/logo.png" alt="" class="img-fluid"></a>
            <nav class="nav-menu d-none d-lg-block">
                <ul>
                <li class="active"><a href="#hero">Início</a></li>
                <li><a href="#about">Soluções</a></li>
                <li><a href="#services">Encontre um consultor</a></li>
                <li><a href="#contact">Contato</a></li>
                </ul>
            </nav>
            <a href="#appointment" class="appointment-btn scrollto" style="margin-right: 10px;">Entrar</a>
            <a href="#appointment" class="appointment-btn scrollto" style="width: 108px; text-align: center; padding-left: 14px;">Inscrever se</a>
            </div>
        </header>
        <section id="hero" class="d-flex align-items-center">
            <div class="container">
            <h1>Bridge Maker</h1>
            <h2>Conectando Soluções</h2>
            <a href="#about" class="btn-get-started scrollto">Inscreva se</a>
            </div>
        </section>
        <main id="main">
            <section id="about" class="why-us">
            <div class="container">

                <div class="row">
                <div class="col-lg-4 d-flex align-items-stretch">
                    <div class="content">
                    <h3>Sou cliente</h3>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in reprehenderit
                        Asperiores dolores sed et. Tenetur quia eos. Autem tempore quibusdam vel necessitatibus optio ad corporis.
                    </p>
                    <div class="text-center">
                        <a href="#" class="more-btn">Inscrever se <i class="bx "></i></a>
                    </div>
                    </div>
                </div>
                <div class="col-lg-8 d-flex align-items-stretch">
                    <div class="icon-boxes d-flex flex-column justify-content-center">
                    <div class="row">
                        <div class="col-xl-4 d-flex align-items-stretch">
                        <div class="icon-box mt-4 mt-xl-0">
                            <i class="bx bx-receipt"></i>
                            <h4>Vantagem 1</h4>
                            <p>Consequuntur sunt aut quasi enim aliquam quae harum pariatur laboris nisi ut aliquip</p>
                        </div>
                        </div>
                        <div class="col-xl-4 d-flex align-items-stretch">
                        <div class="icon-box mt-4 mt-xl-0">
                            <i class="bx bx-cube-alt"></i>
                            <h4>Vantagem 2</h4>
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt</p>
                        </div>
                        </div>
                        <div class="col-xl-4 d-flex align-items-stretch">
                        <div class="icon-box mt-4 mt-xl-0">
                            <i class="bx bx-images"></i>
                            <h4>Vantagem 3</h4>
                            <p>Aut suscipit aut cum nemo deleniti aut omnis. Doloribus ut maiores omnis facere</p>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
                <div class="row">
                <div class="col-lg-4 d-flex align-items-stretch">
                    <div class="content" style="margin-top: 22px;">
                    <h3>Sou consultor</h3>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in reprehenderit
                        Asperiores dolores sed et. Tenetur quia eos. Autem tempore quibusdam vel necessitatibus optio ad corporis.
                    </p>
                    <div class="text-center">
                        <a href="#" class="more-btn">Inscrever se <i class="bx "></i></a>
                    </div>
                    </div>
                </div>
                <div class="col-lg-8 d-flex align-items-stretch">
                    <div class="icon-boxes d-flex flex-column justify-content-center">
                    <div class="row">
                        <div class="col-xl-4 d-flex align-items-stretch">
                        <div class="icon-box mt-4 mt-xl-0">
                            <i class="bx bx-receipt"></i>
                            <h4>Vantagem 1</h4>
                            <p>Consequuntur sunt aut quasi enim aliquam quae harum pariatur laboris nisi ut aliquip</p>
                        </div>
                        </div>
                        <div class="col-xl-4 d-flex align-items-stretch">
                        <div class="icon-box mt-4 mt-xl-0">
                            <i class="bx bx-cube-alt"></i>
                            <h4>Vantagem 2</h4>
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt</p>
                        </div>
                        </div>
                        <div class="col-xl-4 d-flex align-items-stretch">
                        <div class="icon-box mt-4 mt-xl-0">
                            <i class="bx bx-images"></i>
                            <h4>Vantagem 3</h4>
                            <p>Aut suscipit aut cum nemo deleniti aut omnis. Doloribus ut maiores omnis facere</p>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
                <div class="row">
                <div class="col-lg-4 d-flex align-items-stretch">
                    <div class="content" style="margin-top: 22px;">
                    <h3>Sou fornecedor</h3>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in reprehenderit
                        Asperiores dolores sed et. Tenetur quia eos. Autem tempore quibusdam vel necessitatibus optio ad corporis.
                    </p>
                    <div class="text-center">
                        <a href="#" class="more-btn">Inscrever se <i class="bx "></i></a>
                    </div>
                    </div>
                </div>
                <div class="col-lg-8 d-flex align-items-stretch">
                    <div class="icon-boxes d-flex flex-column justify-content-center">
                    <div class="row">
                        <div class="col-xl-4 d-flex align-items-stretch">
                        <div class="icon-box mt-4 mt-xl-0">
                            <i class="bx bx-receipt"></i>
                            <h4>Vantagem 1</h4>
                            <p>Consequuntur sunt aut quasi enim aliquam quae harum pariatur laboris nisi ut aliquip</p>
                        </div>
                        </div>
                        <div class="col-xl-4 d-flex align-items-stretch">
                        <div class="icon-box mt-4 mt-xl-0">
                            <i class="bx bx-cube-alt"></i>
                            <h4>Vantagem 2</h4>
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt</p>
                        </div>
                        </div>
                        <div class="col-xl-4 d-flex align-items-stretch">
                        <div class="icon-box mt-4 mt-xl-0">
                            <i class="bx bx-images"></i>
                            <h4>Vantagem 3</h4>
                            <p>Aut suscipit aut cum nemo deleniti aut omnis. Doloribus ut maiores omnis facere</p>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            </section>
            <section id="contact" class="contact">
            <div class="container">
                <div class="section-title">
                <h2>Contato</h2>
                <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
                </div>
            </div>
            <div class="container">
                <div class="row mt-5">
                <div class="col-lg-8 mt-5 mt-lg-0 centro">
                    <form action="forms/contact.php" method="post" role="form" class="php-email-form">
                    <div class="form-row">
                        <div class="col-md-6 form-group">
                        <input type="text" name="name" class="form-control" id="name" placeholder="Nome" data-rule="minlen:4" data-msg="Escreva no mín. 4 carácteres" />
                        <div class="validate"></div>
                        </div>
                        <div class="col-md-6 form-group">
                        <input type="email" class="form-control" name="email" id="email" placeholder="Email" data-rule="email" data-msg="Por favor, informe um email válido" />
                        <div class="validate"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="subject" id="subject" placeholder="Assunto" data-rule="minlen:4" data-msg="Escreva no mín. 4 carácteres" />
                        <div class="validate"></div>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Escreva algo" placeholder="Mensagem"></textarea>
                        <div class="validate"></div>
                    </div>
                    <div class="mb-3">
                        <div class="loading">Carregando</div>
                        <div class="error-message"></div>
                        <div class="sent-message">Sua mensagem foi enviada. Obrigado!</div>
                    </div>
                    <div class="text-center"><button type="submit">Enviar Mensagem</button></div>
                    </form>
                </div>
                </div>
            </div>
            </section>
        </main>
        <footer id="footer">
            <div class="container d-md-flex py-4">
            <div class="mr-md-auto text-center text-md-left">
                <div class="copyright">
                &copy; Copyright <strong><span>Bridge Maker</span></strong>. Todos os direitos reservados
                </div>
            </div>
            </footer>
        <div id="preloader"></div>
        <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
        <!-- Vendor JS Files -->
        <script src="./vendor/jquery/jquery.min.js"></script>
        <script src="./vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="./vendor/jquery.easing/jquery.easing.min.js"></script>
        <script src="./vendor/php-email-form/validate.js"></script>
        <script src="./vendor/venobox/venobox.min.js"></script>
        <script src="./vendor/waypoints/jquery.waypoints.min.js"></script>
        <script src="./vendor/counterup/counterup.min.js"></script>
        <script src="./vendor/owl.carousel/owl.carousel.min.js"></script>
        <script src="./vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
        <!-- Template Main JS File -->
        <script src="./js/main.js"></script>
    </body>
</html>
