@extends('layouts.app')

@section('content')

<header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">
    <a href="/home#hero" class="logo mr-auto"><img src="./img/logo.png" alt="" class="img-fluid"></a>
    <nav class="nav-menu d-none d-lg-block"> 
        <ul>
        <li class="active"><a href="/home#hero">Início</a></li>
        <li><a href="/home#client">Cliente</a></li>
        <li><a href="/home#consult">Consultor</a></li>
        <li><a href="/home#vendor">Fornecedor</a></li>
        <li><a href="/home#contact">Contato</a></li>
        </ul>
    </nav>
    <a href="/login" class="appointment-btn scrollto" style="margin-right: 10px;">Entrar</a>
    </div>
</header>
    
<main id="main">
    <div class="container">
        <div class="row mt-5">
            <div class="col-lg-8 mt-5 mt-lg-0 centro">
                <style>
                    .uper-title {
                        margin-top: 20%;
                    }
                    .uper {
                        margin-top: 40px;
                    }
                </style>
                <div class="uper-title">
                    <div class="section-title">
                        <h2>Cadastro de cliente</h2>
                        <p>Encontre o sistema perfeito para gerenciar o seu negócio! O BridgeMaker possui uma vasta gama de softwares para os mais diversos nichos de mercado, para que você fique seguro na gestão e informatização do seu estabelecimento.</p>
                    </div>
                </div>
                <div class="card uper">
                    <div class="card-body">
                        @if ( session('success') ) 
                            <div class="alert alert-success">
                                Cliente cadastrado com sucesso!
                            </div><br>
                        @endif
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br>
                        @endif
                        <form method="post" action="{{ route('client.store') }}">
                            @csrf
                            <div class="form-group">
                                <label for="name">Nome *</label>
                                <input type="text" class="form-control" name="name"/>
                            </div>
                            <div class="form-group">
                                <label for="name">Telefone *</label>
                                <input type="text" class="form-control" name="fone"/>
                            </div>
                            <div class="form-group">
                                <label for="price">Email *</label>
                                <input type="email" class="form-control" name="email"/>
                            </div>
                            <div class="form-group">
                                <label for="quantity">Senha *</label>
                                <input type="password" class="form-control" name="password"/>
                            </div>
                            <button type="submit" class="btn btn-primary">Cadastrar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<footer id="footer" style="position:absolute;bottom:0;width:100%;">
    <div class="container d-md-flex py-4">
    <div class="mr-md-auto text-center text-md-left">
        <div class="copyright">
        &copy; Copyright <strong><span>Bridge Maker</span></strong>. Todos os direitos reservados
        </div>
    </div>
</footer>
@endsection