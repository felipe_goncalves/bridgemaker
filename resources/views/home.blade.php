@extends('layouts.app')

@section('content')
<header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">
        <a href="#hero" class="logo mr-auto"><img src="./img/logo.png" alt="" class="img-fluid"></a>
        <nav class="nav-menu d-none d-lg-block"> 
            <ul>
            <li class="active"><a href="#hero">Início</a></li>
            <li><a href="#client">Cliente</a></li>
            <li><a href="#consult">Consultor</a></li>
            <li><a href="#vendor">Fornecedor</a></li>
            <li><a href="#contact">Contato</a></li>
            </ul>
        </nav>
        <a href="/login" class="appointment-btn scrollto" style="margin-right: 10px;">Entrar</a>
    </div>
</header>
<main id="main">
    <section id="hero" class="d-flex align-items-center">
        <div id="client" class="container">
            <h1>Bridge Maker</h1>
            <h2>Conectando Soluções</h2>
        </div>
    </section>
    <section class="why-us">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 d-flex align-items-stretch">
                    <div class="content">
                        <h3>Sou cliente</h3>
                        <p>Encontre o sistema perfeito para gerenciar o seu negócio! O BridgeMaker possui uma vasta gama de softwares para os mais diversos nichos de mercado, para que você fique seguro na gestão e informatização do seu estabelecimento.</p>
                        <div class="text-center">
                            <a href="/client" class="more-btn">Inscrever se <i class="bx "></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 d-flex align-items-stretch">
                    <div class="icon-boxes d-flex flex-column justify-content-center">
                        <div class="row">
                            <div class="col-xl-4 d-flex align-items-stretch">
                            <div class="icon-box mt-4 mt-xl-0">
                                <i class="bx bx-receipt"></i>
                                <h4>Para Todos os Bolsos</h4>
                                <p>Encontre o sistema perfeito para a situação financeira do seu negócio, com opções de licenças permanentes ou mensalidade.</p>
                            </div>
                            </div>
                            <div class="col-xl-4 d-flex align-items-stretch">
                            <div class="icon-box mt-4 mt-xl-0">
                                <i class="bx bx-cube-alt"></i>
                                <h4>Atendimento Personalizado</h4>
                                <p>Com o amparo da nossa rede de consultores certificados, você não terá nenhuma dúvida sobre qual sistema é melhor para o seu negócio.</p>
                            </div>
                            </div>
                            <div class="col-xl-4 d-flex align-items-stretch">
                            <div class="icon-box mt-4 mt-xl-0">
                                <i class="bx bx-images"></i>
                                <h4>100% online</h4>
                                <p>Todas as etapas de implantação do sistema, inclusive o suporte, é 100% online e integrado com a plataforma, facilitando o gerenciamento e usabilidade.</p>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="consult" class="why-us">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 d-flex align-items-stretch">
                    <div class="card" style="max-height: 400px;width: auto;height: auto;">
                        <img class="card-img-top" src="img/consultor.jpeg" alt="consult">
                    </div>
                </div>
                <div class="col-lg-4 d-flex align-items-stretch">
                    <div class="content" style="margin-top: 22px;">
                        <h3>Sou consultor</h3>
                        <p>Forneça o seu serviço de consultoria para uma grande base de clientes e amplie sua presença online se tornando um consultor certificado BridgeMaker! Atenda clientes de todo o Brasil atendendo remotamente os nichos de mercado que você domina.</p>
                        <div class="text-center">
                            <a href="/consultant" class="more-btn">Inscrever se <i class="bx "></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 d-flex align-items-stretch">
                    <div class="icon-boxes d-flex flex-column justify-content-center">
                        <div class="row">
                            <div class="col-xl-4 d-flex align-items-stretch">
                                <div class="icon-box mt-4 mt-xl-0">
                                    <i class="bx bx-receipt"></i>
                                    <h4>Cresça Conosco</h4>
                                    <p>Atenda clientes de todo o Brasil de forma 100% online, atuando para milhares de negócios no conforto da sua casa ou escritório.</p>
                                </div>
                            </div>
                            <div class="col-xl-4 d-flex align-items-stretch">
                                <div class="icon-box mt-4 mt-xl-0">
                                    <i class="bx bx-cube-alt"></i>
                                    <h4>Comissionamento Justo</h4>
                                    <p>No BridgeMaker você pode definir quanto cobra, além da taxa mínima de comissionamento, para maior flexibilidade e controle dos seus ganhos.</p>
                                </div>
                            </div>
                            <div class="col-xl-4 d-flex align-items-stretch">
                                <div class="icon-box mt-4 mt-xl-0">
                                    <i class="bx bx-images"></i>
                                    <h4>Fidelidade</h4>
                                    <p>Evolua e ganhe junto com a BridgeMaker participando de programas de metas com premiações e benefícios aos nossos consultores certificados.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="vendor" class="why-us">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 d-flex align-items-stretch">
                    <div class="card" style="max-height: 400px;width: auto;height: auto;">
                        <img class="card-img-top" src="img/fornecedor.jpg" alt="vendor">
                    </div>
                </div>
                <div class="col-lg-4 d-flex align-items-stretch">
                    <div class="content" style="margin-top: 22px;">
                    <h3>Sou fornecedor</h3>
                    <p>Cresça exponencialmente sua presença de mercado ofertando o seu sistema no BridgeMaker! Cadastre sua solução de software na maior vitrine virtual de sistemas de gestão e atinja clientes em todo o Brasil.</p>
                    <div class="text-center">
                        <a href="/vendors" class="more-btn">Inscrever se <i class="bx "></i></a>
                    </div>
                    </div>
                </div>
                <div class="col-lg-8 d-flex align-items-stretch">
                    <div class="icon-boxes d-flex flex-column justify-content-center">
                        <div class="row">
                            <div class="col-xl-4 d-flex align-items-stretch">
                                <div class="icon-box mt-4 mt-xl-0">
                                    <i class="bx bx-receipt"></i>
                                    <h4>Escalabilidade</h4>
                                    <p>Distribua sua solução no maior marketplace de sistemas de gestão do Brasil, ampliando seus ganhos e base de clientes exponencialmente.</p>
                                </div>
                            </div>
                            <div class="col-xl-4 d-flex align-items-stretch">
                                <div class="icon-box mt-4 mt-xl-0">
                                    <i class="bx bx-cube-alt"></i>
                                    <h4>Gratuito</h4>
                                    <p>A sua empresa não paga nada para disponibilizar o software na plataforma, sendo os custos apenas no comissionamento da compra.</p>
                                </div>
                            </div>
                            <div class="col-xl-4 d-flex align-items-stretch">
                                <div class="icon-box mt-4 mt-xl-0">
                                    <i class="bx bx-images"></i>
                                    <h4>24/7/365</h4>
                                    <p>A BridgeMaker está disponível a todo momento, permitindo o maior alcance possível do seu produto.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="contact" class="contact">
        <div class="container">
            <div class="section-title">
                <h2>Contato</h2>
                <p>Dúvidas? Sugestões? Elogios? Entre em contato com a nossa equipe de especialistas. Estamos prontos para auxiliar você e seu negócio a ir cada vez mais longe.</p>
            </div>
        </div>
        <div class="container">
            <form action="forms/contact.php" method="post" role="form" class="php-email-form">
                <div class="form-row">
                    <div class="col-md-6 form-group">
                        <input type="text" name="name" class="form-control" id="name" placeholder="Nome" data-rule="minlen:4" data-msg="Escreva no mín. 4 carácteres" />
                        <div class="validate"></div>
                    </div>
                    <div class="col-md-6 form-group">
                        <input type="email" class="form-control" name="email" id="email" placeholder="Email" data-rule="email" data-msg="Por favor, informe um email válido" />
                        <div class="validate"></div>
                    </div>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="subject" id="subject" placeholder="Assunto" data-rule="minlen:4" data-msg="Escreva no mín. 4 carácteres" />
                    <div class="validate"></div>
                </div>
                <div class="form-group">
                    <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Escreva algo" placeholder="Mensagem"></textarea>
                    <div class="validate"></div>
                </div>
                <div class="mb-3">
                    <div class="loading">Carregando</div>
                    <div class="error-message"></div>
                    <div class="sent-message">Sua mensagem foi enviada. Obrigado!</div>
                </div>
                <div class="text-center"><button type="submit">Enviar Mensagem</button></div>
            </form>
        </div>
    </section>
</main>
@endsection
@extends('layouts.footer')