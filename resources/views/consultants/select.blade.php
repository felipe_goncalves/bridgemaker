<label for="consultants_id"> {{ 'Consultores' }} *</label>
<select class="form-control" id="consultants_id" name="consultants_id">
    <option value="">{{'Selecione um consultor' }}</option>
    @if( ! $consultants->isEmpty() )
        @foreach( $consultants as $consultant )
            <option value="{{$consultant->id}}">{{$consultant->name}}</option>
        @endforeach
    @endif
</select>