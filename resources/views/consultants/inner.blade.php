@extends('layouts.app')
@section('content')
<header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">
        <a href="#hero" class="logo mr-auto"><img src="./img/logo.png" alt="" class="img-fluid"></a>
        <nav class="nav-menu d-none d-lg-block"> 
            <ul>
                <li class="active"><a href="/home#hero">Início</a></li>
                <li><a href="/home#client">Cliente</a></li>
                <li><a href="/home#consult">Consultor</a></li>
                <li><a href="/home#vendor">Fornecedor</a></li>
                <li><a href="/home#contact">Contato</a></li>
            </ul>
        </nav>
        <a href="/login" class="appointment-btn scrollto" style="margin-right: 10px;">Entrar</a>
    </div>
</header>
<main id="main">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="panel panel-default">
                    <div class="panel-heading resume-heading">
                        <div class="row" style="width: 30rem;">
                            <div class="col-lg-12" style="margin-top: 110px;">
                                <div class="col-xs-12 col-sm-8">
                                    <div class="card card-body">
                                        <ul class="list-group">
                                            <li class="list-group-item"><i class="fa fa-user"></i> {{ session('user') ? session('user')->name : '' }} </li>
                                            <li class="list-group-item"><i class="fa fa-credit-card"></i> Função: {{ session('user') ? session('user')->type : '' }} </li>
                                            <li class="list-group-item"><i class="fa fa-envelope"></i>{{ session('user') ? session('user')->email : '' }} </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                <div class="panel panel-default">
                    <div class="panel-heading resume-heading">
                        <div class="row" style="width: 48rem;">
                            <div class="col-lg-12" style="margin-top: 110px;margin-left: 65px;">
                                <div class="card card-body">
                                    @if ( session('error') ) 
                                        <div class="alert alert-error">
                                            session('error')
                                        </div>
                                    @endif
                                    @if ( session('success-post') ) 
                                        <div class="alert alert-success">
                                            Mensagem enviada com sucesso!
                                        </div>
                                    @else
                                        <br>
                                    @endif
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div><br>
                                    @endif

                                    <ul class="nav nav-tabs mb-4" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="basicInfo-tab" data-toggle="tab" href="#basicInfo" role="tab" aria-controls="basicInfo" aria-selected="true">Clientes</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="connectedServices-tab" data-toggle="tab" href="#connectedServices" role="tab" aria-controls="connectedServices" aria-selected="false">Fornecedores</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content ml-1" id="myTabContent">
                                        <div class="tab-pane fade show active" id="basicInfo" role="tabpanel" aria-labelledby="basicInfo-tab">
                                            <div class="row">
                                                <div class="col-md-12 col-12">
                                                    <form method="post" action="{{ route('inner.store') }}">
                                                        @csrf
                                                        <input type="hidden" class="form-control" name="user" value="{{ $user }}"/>
                                                        <div class="form-group">
                                                            <label for="clients_id"> Cliente *</label>
                                                            <select class="form-control" id="clients_id" name="clients_id">
                                                                <option value="">{{'Selecione um cliente'}}</option>    
                                                                @foreach( $clients as $object )
                                                                    <option value="{{$object->id}}">{{$object->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="name">Mensagem *</label>
                                                            <input type="text" class="form-control" name="menssage"/>
                                                        </div>
                                                        <button type="submit" class="btn btn-primary">Enviar</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="connectedServices" role="tabpanel" aria-labelledby="ConnectedServices-tab">
                                            <div class="row">
                                                <div class="col-md-12 col-12">
                                                    <form method="post" action="{{ route('inner.store') }}">
                                                        @csrf
                                                        <input type="hidden" class="form-control" name="user" value="{{ $user }}"/>
                                                        <div class="form-group">
                                                            <label for="categories_id"> Categoria *</label>
                                                            <select  onchange="loadVendors(this.value)"  class="form-control" id="categories_id" name="categories_id">
                                                                <option value="">{{'Selecione uma categoria'}}</option>
                                                                @foreach( $categories as $object )
                                                                    <option value="{{$object->id}}">{{$object->description}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="content_select_vendors">
                                                                <label for="vendors_id"> Fornecedor *</label>
                                                                <select class="form-control" id="vendors_id" name="vendors_id">
                                                                    <option value="">{{'Selecione um consultor'}}</option>    
                                                                    @foreach( $vendors as $object )
                                                                        <option value="{{$object->id}}">{{$object->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="name">Mensagem *</label>
                                                            <input type="text" class="form-control" name="menssage"/>
                                                        </div>
                                                        <button type="submit" class="btn btn-primary">Enviar</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12" style="padding-bottom: 15px; padding-top: 15px;">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-title mb-4">
                                <div class="d-flex justify-content-start">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <ul class="nav nav-tabs mb-4" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="client-result-tab" data-toggle="tab" href="#client-result" role="tab" aria-controls="client-result"aria-selected="true">Contatos</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content ml-1" id="myTabContent">
                                        <div class="tab-pane fade show active" id="client-result" role="tabpanel"aria-labelledby="client-result-tab">
                                            @php
                                                $i = 1;
                                            @endphp
                                            @foreach( $posts as $consult => $result )
                                                @php
                                                    $i++;
                                                @endphp
                                                <div class="row">
                                                    <div class="col-md-12 col-12">
                                                    Cliente: {{ $consult }}
                                                        <button class="btn btn-primary pull-right"  data-toggle="collapse" href="#collapse{{$i}}" role="button" aria-expanded="false" aria-controls="collapseExample">
                                                            <svg width="1em" height="1em" viewBox="0 0 16 16"class="bi bi-chat" fill="currentColor"xmlns="http://www.w3.org/2000/svg">
                                                                <path fill-rule="evenodd" d="M2.678 11.894a1 1 0 0 1 .287.801 10.97 10.97 0 0 1-.398 2c1.395-.323 2.247-.697 2.634-.893a1 1 0 0 1 .71-.074A8.06 8.06 0 0 0 8 14c3.996 0 7-2.807 7-6 0-3.192-3.004-6-7-6S1 4.808 1 8c0 1.468.617 2.83 1.678 3.894zm-.493 3.905a21.682 21.682 0 0 1-.713.129c-.2.032-.352-.176-.273-.362a9.68 9.68 0 0 0 .244-.637l.003-.01c.248-.72.45-1.548.524-2.319C.743 11.37 0 9.76 0 8c0-3.866 3.582-7 8-7s8 3.134 8 7-3.582 7-8 7a9.06 9.06 0 0 1-2.347-.306c-.52.263-1.639.742-3.468 1.105z" />
                                                            </svg>
                                                            Mensagens
                                                        </button>
                                                    </div>
                                                    <div class="collapse" id="collapse{{$i}}">
                                                        <div class="card card-body">
                                                            <ul class="list-group">
                                                                @foreach( $result as $result )
                                                                    <li class="list-group-item">
                                                                        @if($result['object'] === 'send_consultant') 
                                                                            <i class="fa fa-share"></i>
                                                                        @else 
                                                                            <i class="fa fa-reply"></i>
                                                                        @endif
                                                                        {{ $result['menssage'] }}
                                                                    </li>
                                                                @endforeach
                                                            </ul>    
                                                        </div>
                                                    </div>
                                                </div><hr/>
                                            @endforeach
                                            @php
                                                $i = 1;
                                            @endphp
                                            @foreach( $posts_vendors as $consult => $result )
                                                @php
                                                    $i++;
                                                @endphp
                                                <div class="row">
                                                    <div class="col-md-12 col-12">
                                                        Fornecedor: {{ $consult }}
                                                        <button class="btn btn-primary pull-right"  data-toggle="collapse" href="#collapsevendor{{$i}}" role="button" aria-expanded="false" aria-controls="collapseExample">
                                                            <svg width="1em" height="1em" viewBox="0 0 16 16"class="bi bi-chat" fill="currentColor"xmlns="http://www.w3.org/2000/svg">
                                                                <path fill-rule="evenodd" d="M2.678 11.894a1 1 0 0 1 .287.801 10.97 10.97 0 0 1-.398 2c1.395-.323 2.247-.697 2.634-.893a1 1 0 0 1 .71-.074A8.06 8.06 0 0 0 8 14c3.996 0 7-2.807 7-6 0-3.192-3.004-6-7-6S1 4.808 1 8c0 1.468.617 2.83 1.678 3.894zm-.493 3.905a21.682 21.682 0 0 1-.713.129c-.2.032-.352-.176-.273-.362a9.68 9.68 0 0 0 .244-.637l.003-.01c.248-.72.45-1.548.524-2.319C.743 11.37 0 9.76 0 8c0-3.866 3.582-7 8-7s8 3.134 8 7-3.582 7-8 7a9.06 9.06 0 0 1-2.347-.306c-.52.263-1.639.742-3.468 1.105z" />
                                                            </svg>
                                                            Mensagens
                                                        </button>
                                                    </div>
                                                    <div class="collapse" id="collapsevendor{{$i}}">
                                                        <div class="card card-body">
                                                            <ul class="list-group">
                                                                @foreach( $result as $result )
                                                                    <li class="list-group-item">
                                                                        @if($result['object'] === 'send_consultant') 
                                                                            <i class="fa fa-share"></i>
                                                                        @else 
                                                                            <i class="fa fa-reply"></i>
                                                                        @endif
                                                                        {{ $result['menssage'] }}
                                                                    </li>
                                                                @endforeach
                                                            </ul>    
                                                        </div>
                                                    </div>
                                                </div><hr/>
                                            @endforeach
                                        <div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script>
    function loadVendors(categories_id){
        console.log(categories_id)
        //$(".content_select_vendors").html('<div class="form-group"><label for="categories_id"> Consultor *</label><div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3"><div style="width: 100%;height: 34px;">carregando ...</div></div></div>');
        $.get("/consultant/consultantsByCategororiesId", { 'categories_id': categories_id }, function (data) {
            console.log(data);
            //$(".content_select_vendors").html(data);
        });
    }
</script>