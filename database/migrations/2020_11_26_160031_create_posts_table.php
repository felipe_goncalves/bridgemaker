<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('consultants_id')->nullable();
            $table->foreign('consultants_id')->references('id')->on('consultants');
            $table->integer('clients_id')->nullable();
            $table->foreign('clients_id')->references('id')->on('clients');
            $table->integer('vendors_id')->nullable();
            $table->foreign('vendors_id')->references('id')->on('vendors');
            $table->string('object');
            $table->string('menssage');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
