<?php

use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $count = 0;
        DB::statement("TRUNCATE TABLE categories RESTART IDENTITY CASCADE");
        DB::table('categories')->insert([
            ["id" => ++$count, "status" => 1, "description" => 'Serviços de Restaurantes e Bares',  "created_at" => new Datetime, "updated_at" => new Datetime],
            ["id" => ++$count, "status" => 1, "description" => 'Serviços Automotivos',  "created_at" => new Datetime, "updated_at" => new Datetime],
            ["id" => ++$count, "status" => 1, "description" => 'Serviços de Construção Civil',  "created_at" => new Datetime, "updated_at" => new Datetime],
            ["id" => ++$count, "status" => 1, "description" => 'Serviços de Administração Empresarial',  "created_at" => new Datetime, "updated_at" => new Datetime],
            ["id" => ++$count, "status" => 1, "description" => 'Serviços de Educação',  "created_at" => new Datetime, "updated_at" => new Datetime],
            ["id" => ++$count, "status" => 1, "description" => 'Serviços de Consultoria',  "created_at" => new Datetime, "updated_at" => new Datetime],
            ["id" => ++$count, "status" => 1, "description" => 'Serviços de Entretenimento',  "created_at" => new Datetime, "updated_at" => new Datetime],
            ["id" => ++$count, "status" => 1, "description" => 'Serviços de Farmácia e Saúde',  "created_at" => new Datetime, "updated_at" => new Datetime],
            ["id" => ++$count, "status" => 1, "description" => 'Serviços Financeiros',  "created_at" => new Datetime, "updated_at" => new Datetime],
            ["id" => ++$count, "status" => 1, "description" => 'Serviços de Academia',  "created_at" => new Datetime, "updated_at" => new Datetime],
            ["id" => ++$count, "status" => 1, "description" => 'Serviços de Estética e Beleza',  "created_at" => new Datetime, "updated_at" => new Datetime],
            ["id" => ++$count, "status" => 1, "description" => 'Serviços de Tecnologia',  "created_at" => new Datetime, "updated_at" => new Datetime],
            ["id" => ++$count, "status" => 1, "description" => 'Serviços Jurídicos',  "created_at" => new Datetime, "updated_at" => new Datetime],
            ["id" => ++$count, "status" => 1, "description" => 'Comércio e Produção de Moda',  "created_at" => new Datetime, "updated_at" => new Datetime],
            ["id" => ++$count, "status" => 1, "description" => 'Comércio e Produção de Móveis',  "created_at" => new Datetime, "updated_at" => new Datetime],
            ["id" => ++$count, "status" => 1, "description" => 'Comércio e Produção de Artesanato',  "created_at" => new Datetime, "updated_at" => new Datetime],
            ["id" => ++$count, "status" => 1, "description" => 'Comércio de Alimentos e Bebidas',  "created_at" => new Datetime, "updated_at" => new Datetime],
            ["id" => ++$count, "status" => 1, "description" => 'Comércio de Varejo',  "created_at" => new Datetime, "updated_at" => new Datetime],
            ["id" => ++$count, "status" => 1, "description" => 'Comércio de Equipamentos',  "created_at" => new Datetime, "updated_at" => new Datetime],
        ]);
        DB::statement("ALTER SEQUENCE categories_id_seq RESTART WITH 2");
        DB::statement("VACUUM(FULL) categories");
    }
}