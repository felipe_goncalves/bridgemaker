<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $fillable = ['name', 'email', 'password', 'description', 'logo', 'categories_id'];

    public function category()
    {
        return $this->belongsTo(Category::class, 'categories_id');
    }
}
