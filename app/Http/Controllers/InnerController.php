<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Client;
use App\Vendor;
use App\Consultant;
use App\Category;
use App\Post;

class InnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = session('user');

        if($user) {

            $posts = [];
            $categories = Category::where('status',1)->get();

            switch($user->type)
            {
                case 'Consultor':

                    $vendors = Vendor::get();
                    $clients = Client::get();

                    $result_post = Post::with('client')->whereNotNull('clients_id')->where( ['consultants_id' => $user->id ] )->get();
                    $data_post = new Collection($result_post);
                    $posts = $data_post->groupBy(['client.name'], $preserveKeys = true);

                    $result_vendor = Post::with('vendor')->whereNotNull('vendors_id')->where( ['consultants_id' => $user->id ] )->get();
                    $data_vendor = new Collection($result_vendor);
                    $posts_vendors = $data_vendor->groupBy(['vendor.name'], $preserveKeys = true);

                    return view('consultants.inner', [
                        'vendors' => $vendors,
                        'clients' => $clients,
                        'user' =>  json_encode($user),
                        'posts' => $posts, 
                        'posts_vendors' => $posts_vendors,
                        'categories' => $categories 
                    ]);                    

                break;
                case 'Fornecedor':

                    $consultants = Consultant::get();
                    $result = Post::with('consultant')->where( ['vendors_id' => $user->id ] )->get();
                    $data = new Collection($result);
                    $posts = $data->groupBy(['consultant.name'], $preserveKeys = true);

                    return view('vendors.inner', [
                        'consultants' => $consultants, 
                        'user' =>  json_encode($user),
                        'posts' => $posts, 
                        'categories' => $categories 
                    ]);
                    
                break;
                case 'Cliente':

                    $consultants = Consultant::get();
                    $result = Post::with('consultant')->where( ['clients_id' => $user->id ] )->get();
                    $data = new Collection($result);
                    $posts = $data->groupBy(['consultant.name'], $preserveKeys = true);

                    return view('clients.inner', [
                        'consultants' => $consultants, 
                        'user' =>  json_encode($user), 
                        'posts' => $posts, 
                        'categories' => $categories 
                    ]);
                    
                break;
            }
        }

        return view('login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user = json_decode($request['user']);

        $consultants_id =  $request['consultants_id'] ? $request['consultants_id'] : null ;
        $clients_id =  $request['clients_id'] ? $request['clients_id'] : null ;
        $vendors_id =  $request['vendors_id'] ? $request['vendors_id'] : null ;
        $menssage =  $request['menssage'];

        $object = '';
        switch( $user->type )
        {
            case 'Cliente':
                Post::create(['clients_id' => $user->id, 'consultants_id' => $consultants_id,  'object' => 'send_client', 'menssage' => $menssage ]);
            break;
            case 'Fornecedor':
                Post::create(['vendors_id' => $user->id, 'consultants_id' => $consultants_id,  'object' => 'send_vendor', 'menssage' => $menssage ]);
            break;
            case 'Consultor':
                if($clients_id)
                    Post::create(['consultants_id' => $user->id, 'clients_id' => $clients_id,  'object' => 'send_consultant', 'menssage' => $menssage ]);    
                elseif($vendors_id)
                    Post::create(['consultants_id' => $user->id, 'vendors_id' => $vendors_id,  'object' => 'send_consultant', 'menssage' => $menssage ]);
            break;
        }
        
        return redirect('/inner')
            ->with('user', $user )
            ->with('success', 'Inner is successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
