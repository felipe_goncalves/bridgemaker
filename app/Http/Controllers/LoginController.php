<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Vendor;
use App\Consultant;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $email = $request['email'];
        $password = $request['password'];

        $user = null;

        $client = Client::where(['email'=> $email, 'password' => $password])->first();
        if( $client ){
            $user = $client;
            $user->type = 'Cliente';
        }

        $vendor = Vendor::where(['email'=> $email, 'password' => $password])->first();
        if( $vendor ){
            $user = $vendor;
            $user->type = 'Fornecedor';
        }

        $consultant = Consultant::where(['email'=> $email, 'password' => $password])->first();
        if( $consultant ){
            $user = $consultant;
            $user->type = 'Consultor';
        }

        if($user) {
            return redirect('/inner')
                ->with('user', $user)
                ->with('success', 'Inner is successfully');
        }

        return redirect('/inner')
            ->with('user', null)
            ->with('error', 'Error');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
