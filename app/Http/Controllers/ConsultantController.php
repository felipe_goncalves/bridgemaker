<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Consultant;
use App\Category;

class ConsultantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('consultants.create',['categories' => Category::where('status', 1)->get() ]);
        /* 
        $consultants = Consultant::all();
        return view('index', compact('consultants')); 
        */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('consultants.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required',
            'password' => 'required',
            'categories_id' => 'required'
        ]);
        $consultant = Consultant::create($validatedData);

        return redirect('/consultant')->with('success', 'Consultant is successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $consultant = Consultant::findOrFail($id);

        return view('edit', compact('Consultant'));
    }

    public function consultantsByCategororiesId()
    {
        $categories_id = Input::get('categories_id');
        $consultants = Consultant::where([ 'categories_id' => $categories_id ])->get();
        return view('consultants.select', ['consultants' => $consultants]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required',
            'password' => 'required',
        ]);
        Consultant::whereId($id)->update($validatedData);

        return redirect('/home')->with('success', 'Consultant is successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $consultant = Consultant::findOrFail($id);
        $consultant->delete();

        return redirect('/home')->with('success', 'Consultant is successfully deleted');
    }
}