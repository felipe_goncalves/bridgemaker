<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consultant extends Model
{
    protected $fillable = ['name', 'email', 'password', 'description', 'address', 'categories_id'];

    public function category()
    {
        return $this->belongsTo(Category::class, 'categories_id');
    }
}
