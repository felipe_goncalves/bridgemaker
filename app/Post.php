<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['consultants_id', 'clients_id', 'vendors_id', 'object', 'menssage'];

    public function consultant()
    {
        return $this->belongsTo(Consultant::class, 'consultants_id');
    }

    public function vendor()
    {
        return $this->belongsTo(Vendor::class, 'vendors_id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'clients_id');
    }
}
